package ch.jr.generics;

public class ContextEntityExt extends ContextEntity {
    private String extendedValue;

    public ContextEntityExt(String extendedValue) {
        this.extendedValue = extendedValue;
    }

    public String getExtendedValue() {
        return extendedValue;
    }

    public void setExtendedValue(String extendedValue) {
        this.extendedValue = extendedValue;
    }
}
