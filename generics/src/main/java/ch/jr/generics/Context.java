package ch.jr.generics;

public class Context<T> {

    private String contextName;
    private T t;

    public String getContextName() {
        return contextName;
    }

    public void setContextName(String contextName) {
        this.contextName = contextName;
    }

    public T getContextEntity() {
        return t;
    }

    public void setContextEntity(T t) {
        this.t = t;
    }
}
