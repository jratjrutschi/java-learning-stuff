package ch.jr.generics;

public class ContextEntity {

    private String entityName;
    private int entityValue;

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public int getEntityValue() {
        return entityValue;
    }

    public void setEntityValue(int entityValue) {
        this.entityValue = entityValue;
    }
}
