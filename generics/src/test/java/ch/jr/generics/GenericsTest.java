package ch.jr.generics;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

/**
 * SonarLint motzt "raw use of parameterized class 'Context'", da Context eine untypisierte Klasse ist und die
 * Objekte, welche darin gespeichert werden sollen, typisiert werden müssen. D.h. beim Deklarieren des Objekts
 * <pre>context</pre> sollte mitgegeben werden, welche Art Objekt darin gespeichert werden soll. D.h. z.B.:
 * <pre>
 *     Context<ContextEntity> context;
 * </pre>
 */
class GenericsTest {
    Context context;
    ContextEntity contextEntity;
    ContextEntityExt contextEntityExt;

    @BeforeEach
    public void init() {
        context = new Context<>();
        contextEntity = new ContextEntity();
        contextEntityExt = new ContextEntityExt("2 x 21");

        context.setContextName("context1");
        contextEntity.setEntityName("contextEntity1");
        contextEntity.setEntityValue(1);
    }

    @Test
    void injectContextEntityTest() {
        context.setContextEntity(contextEntity);

        assertAll(() -> {
            assertEquals("context1", context.getContextName());
            assertEquals(
                    "contextEntity1",
                    ((ContextEntity) context.getContextEntity()).getEntityName());
        });

        assertEquals(
                "ch.jr.generics.ContextEntity",
                context.getContextEntity().getClass().getTypeName());
    }

    @Test
    void injectContextEntityExtTest() {
        context.setContextEntity(contextEntityExt);

        assertAll(() -> {
            assertEquals("context1", context.getContextName());
            assertEquals(
                    "2 x 21",
                    ((ContextEntityExt) context.getContextEntity()).getExtendedValue());
        });

        assertEquals(
                "ch.jr.generics.ContextEntityExt",
                context.getContextEntity().getClass().getTypeName());
    }

    @Test
    void notCastingGetEntityNeededTest() {
        context.setContextEntity(contextEntity);

        Object tempContextEntity = context.getContextEntity();
        assertEquals(
                "ch.jr.generics.ContextEntity",
                tempContextEntity.getClass().getTypeName());
    }

    @Test
    void castingGetEntityNeededTest() {
        context.setContextEntity(contextEntity);

        assertDoesNotThrow(() -> {
            ContextEntity contextEntity = (ContextEntity) context.getContextEntity();
        });
    }

    @Test
    void dontCheckin() {
        Map referenceData = new HashMap<String, Object>();

        referenceData.put("a", new Object());
        referenceData.put("b", null);

        System.out.println(referenceData.get("b"));
    }

}
