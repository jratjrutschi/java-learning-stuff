# maven #

module was created by (accepting the default suggestions of maven-cli):

```console
$ mvn archetype:generate -DgroupId=ch.jr -DartifactId=maven
```

## plugins ##

### properties-maven-plugin ###

Read, writes or manipulates maven properties. Example in this project writes properties to file in build directory.
More about that plugin and possible purposes: https://www.mojohaus.org/properties-maven-plugin/usage.html.

Some example implementations: https://github.com/eugenp/tutorials/tree/2de10a3d7053738edf02189cc7ab131d6d909d37/maven-modules.

#### note ####

in pom.xml there are two possible solutions, for the path of the properties file to write. Both work!

1. ${project.build.outputDirectory} -> writes the output file (with the maven properties) to ...\target\classes
2. ${project.resources[0].directory} ->  write the output file to ...\src\main\resources

## profiles ##

there is a profile named "maven-surefire", which handles the FAILING test class TestFail.class. That means: if the profile is not
active, the maven build fails, because the exclusion in pom is missing!

## echo maven properties

Das kann entweder über das `properties-maven-plugin` oder über ein maven-plugin, dass echo unterstützt gemacht werden.

1. `properties-maven-plugin`: Beispiel im [pom.xml](pom.xml). Schreibt z.B. das property `my.second.awesome.property.a.path` in eine im maven-plugin konfigurierte Datei (so als Quickwin) 
2. `maven-antrun-plugin`: Beispiel im [pom.xml](pom.xml). Schreibt die entsprechenden `echo`-statements (z.B. `<echo message="Hello, world"/>`) beim Maven-build auf die Konsole
