package ch.jr.maven;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class TestFail {

    @Test
    void testFail() {
        assertNotEquals(10, Integer.parseInt("11"));
    }

}
