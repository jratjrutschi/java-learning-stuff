package ch.jr.maven;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class PropertiesReaderTest {

    /**
     * Reads a property and checks that's the one expected.
     *
     * @throws IOException if anything goes wrong
     */
    @Test
    void givenPomProperties_whenPropertyRead_thenPropertyReturned() throws IOException {
        //-- arrange
        PropertiesReader reader = new PropertiesReader("properties-from-pom.properties");

        //-- act
        String property = reader.getProperty("my.awesome.property");

        System.out.println(property);

        //-- assert
        assertEquals("property-from-pom", property);
    }

}
