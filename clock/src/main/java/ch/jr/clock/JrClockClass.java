package ch.jr.clock;

import java.time.Clock;
import java.time.Instant;

public class JrClockClass {
    private Clock clock;

    public JrClockClass(Clock clock) {
        this.clock = clock;
    }

    public void method1() {
        Instant now = clock.instant();
        System.out.println(now);
    }
}
