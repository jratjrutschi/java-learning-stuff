package ch.jr.clock;

/**
 * program should depend on input return some information about time and daylight saving time specialities
 *
 * @author Rutschi Jürg
 */
public class App {
    private static App app = null;

    static final String MSG_ERROR_TOO_MANY_ARGUMENTS = "too many arguments";

    /**
     * pass argument, if daylight saving time is on or not (true or false!)
     *
     * @param args standard java main method
     */
    public static void main(String[] args) {
        app = new App();
        app.execute(args);
    }

    private void execute(String[] args) {
        if (args.length <= 1) {
            CalculatorAgent calcAgent = new CalculatorAgent();
            calcAgent.calculateTime(args);
        } else {
            System.err.println(MSG_ERROR_TOO_MANY_ARGUMENTS);
        }
    }
}
