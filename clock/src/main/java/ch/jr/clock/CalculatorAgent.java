package ch.jr.clock;

import java.time.Clock;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class CalculatorAgent {
    private Clock clock;
    private ZoneId zoneId;
    private DstCalculator dstCalculator;

    public CalculatorAgent() {
        zoneId = ZoneId.of("Europe/Zurich");

        setClock();
        dstCalculator = new DstCalculator(this.clock);
    }

    public void setClock() {
        this.clock = Clock.system(this.zoneId);
    }

    public void calculateTime(String[] args) {
        ZonedDateTime zdtToCalculate;

        if (args.length == 1) {
            zdtToCalculate = parseCustomDateTimeString(args[0]);
        } else {
            zdtToCalculate = ZonedDateTime.ofInstant(clock.instant(), clock.getZone());     // ZonedDateTime because this type considers also DST (daylight saving time)
        }

        dstCalculator.printDstDetails(zdtToCalculate);
    }

    ZonedDateTime parseCustomDateTimeString(String customDatetime) {
        String patternString = "dd.MM.yyyy HH:mm:ss";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(patternString)
                .withZone(ZoneId.of("Europe/Zurich"));

        return ZonedDateTime.parse(customDatetime, formatter);
    }

}
