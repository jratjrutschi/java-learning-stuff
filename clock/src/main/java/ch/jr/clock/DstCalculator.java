package ch.jr.clock;

import java.time.Clock;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.zone.ZoneRules;

/**
 * class to learn daylight saving time (DST) calculations with the class Clock from package time of
 * java - native. This is used for better testability. Testability and the Clock is the purpose of this
 * package.
 *
 * @author Rutschi Jürg
 */
public class DstCalculator {
    Clock clock;

    public DstCalculator(Clock clock) {
        this.clock = clock;
    }

    public void printDstDetails(ZonedDateTime zonedDateTime) {
        ZoneRules zr = clock.getZone().getRules();

        boolean dstActive = zr.isDaylightSavings(zonedDateTime.toInstant());

        if (dstActive) {
            System.out.println(zonedDateTime.format(DateTimeFormatter.ISO_LOCAL_DATE) + " is in summer time");
        } else {
            System.out.println(zonedDateTime.format(DateTimeFormatter.ISO_LOCAL_DATE) + " is in winter / normal time");
        }
    }
}
