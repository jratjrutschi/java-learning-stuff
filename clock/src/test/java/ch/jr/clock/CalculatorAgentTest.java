package ch.jr.clock;

import org.junit.jupiter.api.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.format.DateTimeParseException;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorAgentTest {

    private static final String EOL = System.getProperty("line.separator");
    ByteArrayOutputStream bytes;
    PrintStream console;

    @BeforeEach
    public void init() {
        bytes = new ByteArrayOutputStream();
        console = System.out;
        System.setOut(new PrintStream(bytes));
        System.setErr(new PrintStream(bytes));
    }

    @Test
    void calculateTimeTest() {
        String[] actualWinterDateTime = new String[]{"14.12.2020 11:19:00"};
        CalculatorAgent calculatorAgent = new CalculatorAgent();
        calculatorAgent.calculateTime(actualWinterDateTime);

        assertEquals(
                "2020-12-14 is in winter / normal time" + EOL,
                bytes.toString());
    }

    @Test
    void parseCustomDateTimeExcepctionTest() {
        String[] actualWinterDateTime = new String[]{"14.12.2020T11:19:00"};
        CalculatorAgent calculatorAgent = new CalculatorAgent();
        assertThrows(DateTimeParseException.class, () -> calculatorAgent.calculateTime(actualWinterDateTime));
    }

    @AfterEach
    public void cleanup() {
        System.setOut(console);
    }

}
