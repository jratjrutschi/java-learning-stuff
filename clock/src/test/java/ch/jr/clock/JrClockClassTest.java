package ch.jr.clock;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;

import static org.mockito.Mockito.*;

class JrClockClassTest {

    private JrClockClass jrClockClass;

    private Clock mock;

    @BeforeEach
    public void setUp() {
        mock = mock(Clock.class);
        jrClockClass = new JrClockClass(mock);
    }

    @DisplayName("test method should be renamed!")
    @Test
    void ensureWorksAsDefined() {
        Mockito.when(mock.instant()).thenReturn(Instant.now());     // Instant is UTC be definition
        // further good information: https://stackoverflow.com/questions/32437550/whats-the-difference-between-instant-and-localdatetime/32443004#32443004

        jrClockClass.method1();

        Mockito.verify(mock, times(1)).instant();

        when(mock.instant()).thenReturn(Instant.from(Instant.parse("2077-11-01T11:11:11.111Z")
                .atZone(ZoneId.of("Europe/Zurich"))));

        jrClockClass.method1();

        Mockito.verify(mock, times(2)).instant();
    }

}
