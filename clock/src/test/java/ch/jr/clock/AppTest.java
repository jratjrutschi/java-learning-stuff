package ch.jr.clock;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Date;
import java.util.TimeZone;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Unit test for simple App.
 */
class AppTest {
    private static final String EOL = System.getProperty("line.separator");
    ByteArrayOutputStream bytes;
    PrintStream console;

    @BeforeEach
    public void init() {
        bytes = new ByteArrayOutputStream();
        console = System.out;
        System.setOut(new PrintStream(bytes));
        System.setErr(new PrintStream(bytes));
    }

    @Test
    void validateTooManyArgsTest() {
        String[] args = {"arg1", "arg2"};

        App.main(args);

        assertEquals(App.MSG_ERROR_TOO_MANY_ARGUMENTS + EOL, bytes.toString());
    }

    @Test
    void noArgsTest() {
        String[] args = {};
        DateFormat expectedDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String expectedDate = expectedDateFormat.format(new Date());

        boolean inDaylightTime = TimeZone.getTimeZone(ZoneId.of("Europe/Zurich")).inDaylightTime(new Date());

        App.main(args);

        if (inDaylightTime) {
            assertEquals(String.format("%s is in summer time" + EOL, expectedDate), bytes.toString());
        } else {
            assertEquals(String.format("%s is in winter / normal time" + EOL, expectedDate), bytes.toString());
        }

    }

    @AfterEach
    public void cleanup() {
        System.setOut(console);
    }
}
