package ch.jr.clock;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DstCalculatorTest {
    private static final String EOL = System.getProperty("line.separator");
    ByteArrayOutputStream bytes;
    PrintStream console;

    ZonedDateTime zdtSummer;
    ZonedDateTime zdtWinter;

    @BeforeEach
    public void init() {
        bytes = new ByteArrayOutputStream();
        console = System.out;
        System.setOut(new PrintStream(bytes));
        System.setErr(new PrintStream(bytes));

        zdtSummer = ZonedDateTime.parse("2020-06-06T12:00:00.00Z[Europe/Zurich]");
        zdtWinter = ZonedDateTime.parse("2020-11-11T16:00:00.00Z[Europe/Zurich]");
    }

    @DisplayName("daylight saving time in winter test")
    @Test
    void dstOffWinterTest() {
        Clock clock = Clock.fixed(Instant.now(), ZoneId.of("Europe/Zurich"));
        DstCalculator dstCalculator = new DstCalculator(Clock.systemDefaultZone());

        dstCalculator.printDstDetails(zdtWinter);

        assertEquals(
                String.format("2020-11-11 is in winter / normal time%n", EOL),
                bytes.toString());
    }

    @DisplayName("daylight saving time in summer test")
    @Test
    void dstOnSummerTest() {
        Clock clock = Clock.fixed(Instant.now(), ZoneId.of("Europe/Zurich"));
        DstCalculator dstCalculator = new DstCalculator(clock);

        dstCalculator.printDstDetails(zdtSummer);

        assertEquals(String.format("2020-06-06 is in summer time%n", EOL),
                bytes.toString());
    }

    @AfterEach
    public void cleanup() {
        System.setOut(console);
    }

}
