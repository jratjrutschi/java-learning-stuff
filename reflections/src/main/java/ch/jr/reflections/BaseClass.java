package ch.jr.reflections;

import ch.jr.reflections.annotation.JrAnn;

public class BaseClass {
    public int baseInt;

    @JrAnn(name = "string in annotation von method3")
    private static void method3() {
        System.out.println("method3");
    }

    public int method4() {
        System.out.println("method4");
        return 0;
    }

    public static int method5() {
        System.out.println("method5");
        return 0;
    }

    void method6() {
        System.out.println("method6");
    }

    // inner public class
    public class BaseClassInnerClass {
    }

    //member public enum
    public enum BaseClassMemberEnum {}
}
