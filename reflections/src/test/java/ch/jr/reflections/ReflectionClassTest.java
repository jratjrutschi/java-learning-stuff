package ch.jr.reflections;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ReflectionClassTest {

    @DisplayName("get class and test canonical name")
    @Test
    void classObjectfromClassTest() {
        Class baseClass = BaseClass.class;
        assertEquals("ch.jr.reflections.BaseClass", baseClass.getCanonicalName());
    }

    @DisplayName("get class from Name and test canonical name")
    @Test
    void classObjectFromName() throws ClassNotFoundException {
        Class baseClass = Class.forName("ch.jr.reflections.BaseClass");
        assertEquals("ch.jr.reflections.BaseClass", baseClass.getCanonicalName());
    }

    @Test
    void getClassesMethodTest() throws ClassNotFoundException {
        Class baseClass = Class.forName("ch.jr.reflections.BaseClass");
        Class<?>[] classes = baseClass.getClasses();

        assertAll(() -> {
            assertEquals(2, classes.length);
            assertEquals(2, Arrays.stream(classes)
                    .filter(elem -> "ch.jr.reflections.BaseClass.BaseClassMemberEnum".equals(elem.getCanonicalName()) ||
                            "ch.jr.reflections.BaseClass.BaseClassInnerClass".equals(elem.getCanonicalName()))
                    .count());
        });
    }

}
