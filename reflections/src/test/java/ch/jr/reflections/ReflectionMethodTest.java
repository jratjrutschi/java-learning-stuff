package ch.jr.reflections;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class ReflectionMethodTest {
    private static Class BASE_CLASS;

    @BeforeAll
    static void init() throws ClassNotFoundException {
        BASE_CLASS = Class.forName("ch.jr.reflections.BaseClass");
    }

    @Test
    void methodMofierEmptyTest() {
        Method[] methods = BASE_CLASS.getDeclaredMethods();
        Arrays.stream(methods).forEach(System.out::println);

        Modifier.toString(methods[0].getModifiers());

        Method method = Arrays.stream(methods)
                .filter(e -> e.getName().contains("method6"))
                .findFirst()
                .get();

        // null because there is nothing written
        assertTrue(Modifier.toString(method.getModifiers()).isEmpty());
    }

    @Test
    void methodMofierPublicTest() {
        Method[] methods = BASE_CLASS.getDeclaredMethods();
        Arrays.stream(methods).forEach(System.out::println);

        Modifier.toString(methods[0].getModifiers());

        Method method = Arrays.stream(methods)
                .filter(e -> e.getName().contains("method4"))       // also method5 is public!
                .findFirst()
                .get();

        // null because there is nothing written
        assertTrue(Modifier.isPublic(method.getModifiers()));
    }
}
