package ch.jr.reflections;

import ch.jr.reflections.annotation.JrAnn;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class ReflectionAnnotationTest {
    private static Class BASE_CLASS;

    @BeforeAll
    static void init() throws ClassNotFoundException {
        BASE_CLASS = Class.forName("ch.jr.reflections.BaseClass");
    }


    @Test
    void getAllAnnotationsTest() {
        Method[] methods = BASE_CLASS.getDeclaredMethods();
        Method[] annotatedMethods = Arrays.stream(methods)
                .filter(m -> m.isAnnotationPresent(JrAnn.class))
                .toArray(Method[]::new);

        Arrays.stream(annotatedMethods)
                .forEach(method -> System.out.println("method: \"" + method.getName() +
                        "\" value in annotation: \"" + method.getAnnotation(JrAnn.class).name() + "\""));

        assertAll(() -> {
            assertEquals(1, annotatedMethods.length);
            assertEquals("method3", annotatedMethods[0].getName());
        });
    }

}
