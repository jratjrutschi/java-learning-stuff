#!/bin/bash

# some commennt

## Testing setting the java version (first set to Java version 11, and then to 8)

export JAVA_HOME='/c/ieu/java/openjdk-11'
export PATH=$JAVA_HOME/bin:$PATH

echo '### Java Version should be 11!'
java -version
echo
echo

export JAVA_HOME='/c/ieu/java/openjdk-1.8'
export PATH=$JAVA_HOME/bin:$PATH

echo '### Java Version should be 8!'
java -version
echo
echo
