# README #

## Pending learnings

* add cucumber tests (examples in apv-angebotprofil-service)

## What is this repository for? ##

* This project is to learn several aspects of **java** and **maven**, i want/have to know. 
* Version 0.3

### Version history

0.3 -> filesystem methods added
0.2 -> some improvements plus tests fixed  
0.1 -> first version

## How was the project created ##
generate Parent POM:
```console
$ mvn archetype:generate -DgroupId=ch.jr -DartifactId=java-learning-stuff
```
(java-learning-stuff must not exist. A folder with the artifactId will be created!)

Before you are able to create submodules, change the packaging to pom: `<packaging>pom</packaging>` **<-- Warum?**

```console
$ mvn archetype:generate -DgroupId=ch.jr -DartifactId=module-name-1
$ mvn archetype:generate -DgroupId=ch.jr -DartifactId=module-name-2
```

If you use IntelliJ you have to add the new module to the project structure. Import the new module in the "Project Structure" under "Project Settings" in the section "Modules".

## How do I get set up? ##

* Intellij:
    * New project from existing sources
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions
