# functional interfaces & Lambda

## functional interfaces
Kommen aus dem Paket: `java.util.function`  
Funktionen realisieren Abbildungen, und da es verschiedene Arten von Abbildungen geben kann, bietet die Java-Standardbibliothek im Paket java.util.function für die häufigsten Fälle funktionale Schnittstellen an.

#### Links
https://www.geeksforgeeks.org/functional-interfaces-java/?ref=lbp
https://www.baeldung.com/java-8-functional-interfaces
https://openbook.rheinwerk-verlag.de/javainsel/12_005.html#u12.5.5 -> hier vor allem interessant Kapitel "Einen eigenen Wrapper-Consumer schreiben"
https://www.dontpanicblog.co.uk/2020/11/28/execute-around-idiom-in-java/ _Execute Around idiom_ in Java (das Thema "Execute-around-Method-Muster" auch im Kapitel oben erwähnt)


### Consumer
### Supplier
### Predicate
### Function
### BiFunction
Eigentlich "BiFunction Interface methods – apply() and addThen()" von hier <https://www.geeksforgeeks.org/java-bifunction-interface-methods-apply-and-addthen/>.

Dabei sind die Buchstaben wichtig:
- T: bezeichnet den Typ des ersten Arguments an die Funktion
- U: bezeichnet den Typ des zweiten Arguments an die Funktion
- R: bezeichnet den Rückgabewert der Funktion

Functions in BiFunction Interface
1. apply()

Syntax und Anwendung:
```java
// --- Syntax
R apply(T t, U u);

// --- Anwendung
// BiFunction to add 2 numbers
BiFunction<Integer, Integer, Integer> add = (a, b) -> a + b;

// Implement add using apply()
        System.out.println("Sum = " + add.apply(2, 3));
```

2. addThen()

...kann ich dann anschauen, wenns soweit ist!