package ch.jr.lambda.functionalinterface;

import java.util.function.IntConsumer;

/**
 * Beispiel zum Zeigen wie das Consumer Interface (functional interface) funktioniert.
 *
 * Wenn du einen JAVA Lambda Ausdruck, mit einem Objekt als Parameter und keinem Rückgabewert an eine Methode übergeben
 * möchtest, dann kannst du das Consumer Interface verwenden und dir die Erstellung eines eigenen funktionalen
 * Interfaces sparen.
 */
public class UsingConsumer {

    public static void showHow() {
        // Consumer to display a number
        IntConsumer display = a -> {
            String output = a + " war die Zahl, die eingegeben wurde!";
            System.out.println(output);
        };

        // oder einfach nur:
        // Consumer<Integer> display = a -> System.out.println(a);

        // Implement display using accept()
        display.accept(10);
    }

}
