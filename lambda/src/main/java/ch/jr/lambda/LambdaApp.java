package ch.jr.lambda;

import ch.jr.lambda.functionalinterface.UsingConsumer;
import ch.jr.lambda.functionalinterface.UsingFunction;

/**
 * Also Merke: Wenn du einen JAVA Lambda Ausdruck, mit einem Objekt als Parameter und keinem Rückgabewert an eine
 * Methode übergeben möchtest, dann kannst du das Consumer Interface verwenden und dir die Erstellung eines eigenen
 * funktionalen Interfaces sparen.
 *
 * java.util.function Paket noch viele weitere funktionale Interfaces, welche uns das Definieren eigener Interfaces
 * oft ersparen.
 */
public class LambdaApp
{
    public static void main( String[] args )
    {
        UsingConsumer.showHow();
        UsingFunction.showHow();
    }
}
