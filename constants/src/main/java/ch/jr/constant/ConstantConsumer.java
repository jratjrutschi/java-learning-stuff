package ch.jr.constant;

public class ConstantConsumer {

    public void testConstantAccess() {
        System.out.println("PUBLIC_CONSTANT_STR is accessible, value: " + App.PUBLIC_CONSTANT_STR);
        System.out.println("private constant is not visible!");
        System.out.println("output of enum \"DIVIDE\": " + App.Operation.DIVIDE);
    }

}
