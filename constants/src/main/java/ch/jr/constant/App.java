package ch.jr.constant;

/**
 * Main class for all the stuff with constants, learned espacially from
 * https://www.baeldung.com/java-constants-good-practices
 *
 * @author Rutschi Jürg
 */
public class App {
    /*
    - constants are static and final
        - static: variable doesn't belong to an object, is independent. They exist one time (independent from amount
            of instances of the class
        - final: entity that can only be assigned once
    - name should be all capital letters with words separated
    - final: Felder mit Modifier final können nicht nur bei Deklaration ihren Wert erhalten, sondern auch noch
        im Konstruktor, aber nicht später!
        Dadurch ist es möglich, im Konstruktor übergebene Parameter in ihnen zu speichern und gleichzeitig
            sicherzustellen, dass sich ihr Wert danach nicht mehr verändert.
     */
    private static final int PRIVATE_CONSTANT_COUNT = 42;
    public static final String PUBLIC_CONSTANT_STR = "jr@jrutschi.ch";

    private final String finalString;

    public enum Operation {
        ADD,
        SUBTRACT,
        DIVIDE,
        MULTIPLY
    }

    public App() {
        finalString = "Musik Song von Miley Cyrus";
    }

    public static void main(String[] args) {
        System.out.println("i can see the public constanct: " + PUBLIC_CONSTANT_STR +
                " and the private constant: " + PRIVATE_CONSTANT_COUNT);

        new ConstantConsumer().testConstantAccess();
    }

    /**
     * method is just to proof, what happens, when I try to assign a value to a final variable after the initialization
     * or the constructor
     */
    private void spaeteMethode() {
        // IDE and maven report: "Cannot assign a value to final variable"
//        finalString = "adalbert";
    }
}
