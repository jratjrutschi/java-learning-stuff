package ch.jr.constant;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit test for all classes in this project
 */
public class ConstantsTest {

    /**
     * test no errors in class ConstantConsumer
     */
    @Test
    public void constantConsumerTest()
    {
        new ConstantConsumer().testConstantAccess();
    }

    @Test
    public void publicConstantAccessibleTest() {
        String expected = "jr@jrutschi.ch";
        assertEquals(App.PUBLIC_CONSTANT_STR, expected);
    }

    @Test
    public void publicEnumTest() {
        String expected = "DIVIDE";
        assertEquals(expected, App.Operation.DIVIDE.toString());
    }

}
