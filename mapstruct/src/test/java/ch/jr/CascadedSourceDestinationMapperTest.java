package ch.jr;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CascadedSourceDestinationMapperTest {

    CascadedSourceDestinationMapper mapper;

    @BeforeEach
    void setUp() {
        mapper = new CascadedSourceDestinationMapperImpl();
    }

    @Test
    void sourceToDestination() {
        //-- arrange
        final CascadedSource input = createInputDto();

        //-- act
        final CascadedDestination result = mapper.sourceToDestination(input);

        //-- assert
        assertFullyMappedEntity(input, result);
    }

    private void assertFullyMappedEntity(final CascadedSource input, final CascadedDestination result) {
        assertEquals(input.getName(), result.getName());
        assertEquals(input.getSimpleSource().getName(), result.getSimpleDestination().getName());
        assertEquals(input.getSimpleSource().getDescription(), result.getSimpleDestination().getDescription());
    }

    private CascadedSource createInputDto() {
        final SimpleSource simpleSource = SimpleSource.builder().name("SimpleName").description("SimpleDescription").build();
        return CascadedSource.builder().name("name").simpleSource(simpleSource).build();
    }
}