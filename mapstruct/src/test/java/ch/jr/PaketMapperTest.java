package ch.jr;

import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PaketMapperTest {
    private final PaketMapper mapper = Mappers.getMapper(PaketMapper.class);
    Adresse absender = Adresse.builder()
            .vorname("absender-abc")
            .nachname("absender-def")
            .strasse("strasse-abc")
            .hausnr("22b")
            .plz(3123)
            .ort("Belp")
            .build();
    Adresse empfaenger = Adresse.builder()
            .vorname("empfaenger-abc")
            .nachname("empfaenger-def")
            .strasse("strasse-empfaenger-def")
            .hausnr("44c")
            .plz(3007)
            .ort("Bern")
            .build();

    @Test
    public void givenSourceToDestination_whenMaps_thenCorrect() {
        //-- arrange
        Paket paket = new Paket(absender, empfaenger, PaketGroesse.KLEIN, Lieferzeit.SCHNELL);

        //-- act
        Poschtpaeckli poschtpaeckli = mapper.mapToPoschtpaeckli(paket);

        //-- assert
        assertEquals(poschtpaeckli.getPaeckliEmpfaenger().vorname, paket.getEmpfaenger().vorname);
        assertEquals(poschtpaeckli.getLieferzeit(), paket.getLieferzeit().getName());
        assertEquals("A-Post", poschtpaeckli.getLieferzeit());
        assertEquals(poschtpaeckli.getPaketGroesse(), paket.getPaketTyp().name());
    }

    @Test
    public void givenDestinationToSource_whenMaps_thenCorrect() {
        //-- arrange
        Poschtpaeckli poschtpaeckli = new Poschtpaeckli(absender, empfaenger, "GROSS", "NORMAL");

        //-- act
        Paket paket = mapper.mapToPaket(poschtpaeckli);

        //-- assert
        assertEquals(paket.getEmpfaenger().vorname, poschtpaeckli.getPaeckliEmpfaenger().vorname);
        assertEquals(paket.getLieferzeit().name(), poschtpaeckli.getLieferzeit());
        assertEquals("B-Post", paket.getLieferzeit().getName());
        assertEquals(paket.getPaketTyp().name(), poschtpaeckli.getPaketGroesse());
    }

}