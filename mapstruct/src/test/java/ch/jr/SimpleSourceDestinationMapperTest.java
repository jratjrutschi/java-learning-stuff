package ch.jr;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

public class SimpleSourceDestinationMapperTest {
    private final SimpleSourceDestinationMapper mapper = Mappers.getMapper(SimpleSourceDestinationMapper.class);

    @Test
    public void givenSourceToDestination_whenMaps_thenCorrect() {
        //-- arrange
        final SimpleSource simpleSource = SimpleSource.builder().name("SourceName").description("SourceDescription").build();

        //-- act
        final SimpleDestination destination = mapper.sourceToDestination(simpleSource);

        //-- assert
        assertEquals(simpleSource.getName(), destination.getName());
        assertEquals(simpleSource.getDescription(), destination.getDescription());
    }

    @Test
    public void givenDestinationToSource_whenMaps_thenCorrect() {
        //-- arrange
        final SimpleDestination destination =
                SimpleDestination.builder().name("DestinationName").description("DestinationDescription").build();

        //-- act
        final SimpleSource source = mapper.destinationToSource(destination);

        //-- assert
        assertEquals(destination.getName(), source.getName());
        assertEquals(destination.getDescription(), source.getDescription());
    }
}
