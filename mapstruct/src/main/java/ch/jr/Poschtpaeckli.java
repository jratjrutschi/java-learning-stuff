package ch.jr;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class Poschtpaeckli {
    Adresse paeckliAbsender;
    Adresse paeckliEmpfaenger;
    String paketGroesse;
    String lieferzeit;
}
