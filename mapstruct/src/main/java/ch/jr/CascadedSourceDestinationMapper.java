package ch.jr;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(uses = SimpleSourceDestinationMapper.class)
public interface CascadedSourceDestinationMapper {

    @Mapping(target = "simpleDestination", source = "simpleSource")
    CascadedDestination sourceToDestination(CascadedSource source);
}
