package ch.jr;

import org.mapstruct.Mapper;

@Mapper
public interface SimpleSourceDestinationMapper {

    // hier wird keine @Mapping Annotation benötigt, da die Felder in source und desstination gleich heissen
    SimpleDestination sourceToDestination(SimpleSource source);

    SimpleSource destinationToSource(SimpleDestination destination);
}
