package ch.jr;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;
import org.mapstruct.ValueMapping;
import org.mapstruct.ValueMappings;

@Mapper
public interface PaketMapper {
    @Mapping(target = "paeckliAbsender", source = "absender")
    @Mapping(target = "paeckliEmpfaenger", source = "empfaenger")
    @Mapping(target = "paketGroesse", source = "paketTyp")
    @Mapping(target = "lieferzeit", expression = "java(paket.lieferzeit.getName())")
    Poschtpaeckli mapToPoschtpaeckli(final Paket paket);

    @Mapping(target = "absender", source = "paeckliAbsender")
    @Mapping(target = "empfaenger", source = "paeckliEmpfaenger")
    @Mapping(target = "paketTyp", source = "paketGroesse")
    @ValueMappings({
            @ValueMapping(target = "lieferzeit.NORMAL", source = "B-Post"),
            @ValueMapping(target = "lieferzeit.SCHNELL", source = "A-Post"),
            @ValueMapping(source = MappingConstants.ANY_REMAINING, target = "B-Post")
    })
    Paket mapToPaket(final Poschtpaeckli poschtpaeckli);
}
