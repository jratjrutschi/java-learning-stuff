package ch.jr;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldNameConstants;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@FieldNameConstants
public class CascadedDestination {
    private String name;
    private SimpleDestination simpleDestination;
}
