package ch.jr;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldNameConstants;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@FieldNameConstants
public class CascadedSource {
    private String name;
    private SimpleSource simpleSource;
}
