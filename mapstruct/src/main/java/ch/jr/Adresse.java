package ch.jr;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Adresse {
    String vorname;
    String nachname;
    String strasse;
    String hausnr;
    int plz;
    String ort;
}
