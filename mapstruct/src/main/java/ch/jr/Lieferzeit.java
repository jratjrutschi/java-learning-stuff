package ch.jr;

import lombok.Getter;

@Getter
public enum Lieferzeit {
    SCHNELL("A-Post"), NORMAL("B-Post");

    private final String name;

    Lieferzeit(final String name) {
        this.name = name;
    }
}
