package ch.jr;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Paket {
    Adresse absender;
    Adresse empfaenger;
    PaketGroesse paketTyp;
    Lieferzeit lieferzeit;
}
