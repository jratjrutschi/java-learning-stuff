package ch.jr;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@FieldNameConstants
public class SimpleSource {
    private String name;
    private String description;
}
