# Module Generieren

`mvn archetype:generate -DgroupId=ch.jr -DartifactId=mapstruct`

## Informationen

https://www.baeldung.com/mapstruct

Add the annotationProcessorPaths section to the configuration part of the maven-compiler-plugin plugin in pom.xml. **The
mapstruct-processor is used to generate the mapper implementation during the build**

### enum

Die Erstellung und Konstruktion vom enum `Lieferzeit` ist etwas speziell, aber interessant geraten :-)

## Offenes

- [x] Warum heissen die Tests Integration-Tests? Ist das im angebotprofil- paket-Service auch so? Nein, die heissen in den
  Mobiliar-Services nie *IntegrationTest. Deshalb habe ich die Tests umbenannt!