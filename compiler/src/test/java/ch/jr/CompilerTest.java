package ch.jr;

import static org.junit.jupiter.api.Assertions.assertEquals;

import javax.tools.JavaFileObject;

import org.junit.jupiter.api.Test;

import com.google.common.collect.ImmutableList;
import com.google.testing.compile.Compilation;
import com.google.testing.compile.CompilationSubject;
import com.google.testing.compile.Compiler;
import com.google.testing.compile.JavaFileObjects;

public class CompilerTest {

    @Test
    void testLearningPewpewProcessor() {
        PewpewProcessor processor = new PewpewProcessor();
        JavaFileObject targetFile = JavaFileObjects.forResource("ExamplePewpew.java");

        Compilation compilation = Compiler.javac()
                .withProcessors(processor)
                .compile(targetFile);

        CompilationSubject.assertThat(compilation).succeeded();

        ImmutableList<JavaFileObject> generatedFiles = compilation.generatedFiles();

        assertEquals(1, generatedFiles.size());
    }

}
