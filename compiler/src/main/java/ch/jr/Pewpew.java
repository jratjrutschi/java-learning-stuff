package ch.jr;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
public @interface Pewpew {
    String named() default "";
}
