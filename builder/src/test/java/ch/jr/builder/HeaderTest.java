package ch.jr.builder;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Unit test for Header Dto in builder pattern
 */
public class HeaderTest {

    @Test
    void getHeaderTest() {
        //-- arrange
        String headerContent = "header example";

        //-- act
        Header header = Header.builder()
                .withHeader(headerContent)
                .build();

        //-- assert
        assertEquals(headerContent, header.getHeader());
    }
}
