package ch.jr.builder;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BodyTest {

    @Test
    void getBodyTest() {
        //-- arrange
        String bodyContent = "body example";

        //-- act
        Body body = new Body.Builder()
                .withBody(bodyContent)
                .build();

        //-- assert
        assertEquals(bodyContent, body.getBody());

    }
}