package ch.jr.builder;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RequestDtoTest {
    private static final String HEADER_STRING = "header string";
    private static final String BODY_STRING = "body string";

    RequestDto requestDto;
    Body body;
    Header header;

    @BeforeEach
    void init() {
        this.header = Header.builder()
                .withHeader(HEADER_STRING)
                .build();
        this.body = Body.builder()
                .withBody(BODY_STRING)
                .build();
        this.requestDto = RequestDto.builder()
                .withHeader(this.header)
                .withBody(this.body)
                .build();
    }

    @Test
    void getHeaderTest() {
        assertEquals(HEADER_STRING, requestDto.getHeader().getHeader());
    }

    @Test
    void getBodyTest() {
        assertEquals(BODY_STRING, requestDto.getBody().getBody());
    }

    @Test
    void toStringTest() {
        assertAll(
                () -> assertTrue(requestDto.toString().contains("RequestDto")),
                () -> assertTrue(requestDto.toString().contains(HEADER_STRING)),
                () -> assertTrue(requestDto.toString().contains(BODY_STRING))
        );
    }
}