package ch.jr.builder;

/**
 * Builder pattern parent class
 */
public class RequestDto {
    private Header header;
    private Body body;

//    private RequestDto() { }    // Default Konstruktor v.a. für Objekt-Mapping mit Jackson

    public Header getHeader() {
        return header;
    }

    public Body getBody() {
        return body;
    }

    // Keine Setters! Wir wollen, dass der Zustand über die Builder-Methoden gesetzt wird!

    public static RequestDto.Builder builder() {
        return new RequestDto.Builder();
    }

    public static final class Builder {
        private final RequestDto instance;

        public Builder() {
            instance = new RequestDto();
        }

        public Builder withHeader(final Header header) {
            instance.header = header;
            return this;
        }

        public Builder withBody(final Body body) {
            instance.body = body;
            return this;
        }

        public RequestDto build() {
            return instance;
        }
    }

    @Override
    public String toString() {
        // Wenn mehr String-concatenation verwendet wird, sollte auf Performance Gründen StringBuilder verwendet werden
        return "RequestDto [header='" + header.toString() + "', body='" + body.toString() + "']";
    }

    /*
      exmaple implementation of toJson method analog toString! This need Jackson-Libraries (e.g.: groupId:
      com.fasterxml.jackson.core, artifactId: jackson-databind)

      @return String in json format of this object
     */
//    public String toJson() {
//        ObjectMapper mapper = new ObjectMapper();
//        try {
//            return mapper.writeValueAsString(this);
//        } catch (JsonProcessingException e) {
//            e.printStackTrace();
//        }
//    }
}
