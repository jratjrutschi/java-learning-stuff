package ch.jr.builder;

public class Body {
    private String body;

    public String getBody() {
        return body;
    }

    // Keine Setters! Wir wollen, dass der Zustand über die Builder-Methoden gesetzt wird!

    public static Body.Builder builder() {
        return new Body.Builder();
    }

    public static final class Builder {
        private final Body instance;

        public Builder() {
            instance = new Body();
        }

        public Body.Builder withBody(final String body) {
            instance.body = body;
            return this;
        }

        public Body build() {
            return instance;
        }
    }

    @Override
    public String toString() {
        return "Body [body='" + body + "']";
    }
}
