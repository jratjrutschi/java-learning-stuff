package ch.jr.builder;

public class Header {
    private String header;  // TODO sollte Schlüssel-Werte-Paare enthalten (z.B. "x-caller", "ich")

//    private Header() {} // Default Konstruktor v.a. für Objekt-Mapping mit Jackson
//    private Header(Builder builder) {
//        this.header = builder.header;
//    }

    public String getHeader() {
        return header;
    }

    // Keine Setters! Wir wollen, dass der Zustand über die Builder-Methoden gesetzt wird!

    public static Header.Builder builder() {
        return new Header.Builder();    // das bedingt jedoch eine Instanz in der Builder Klasse, welche im Konstruktur vom Builder zurückgegeben werden muss.
    }

    public static final class Builder {
        /* Wird nicht mehr benötigt, da die Parent-Klasse referenziert ist über die `instance`.
        Gefällt mir sehr gut! */
//        private String header;

        private final Header instance;

        public Builder() {
            instance = new Header();
        }

        public Header.Builder withHeader(final String header) {
            instance.header = header;
            return this;
        }

        public Header build() {
            return instance;
        }
    }

    @Override
    public String toString() {
        // Wenn mehr String-concatenation verwendet wird, sollte auf Performance Gründen StringBuilder verwendet werden
        return "Header [header='" + header + "']";
    }
}
