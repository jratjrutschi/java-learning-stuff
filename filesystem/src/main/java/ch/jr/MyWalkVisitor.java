package ch.jr;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Iterator;

public class MyWalkVisitor {
    public MyWalkVisitor(String myPath) {
        walkPath(myPath);
    }

    private void walkPath(String myPath) {
        Path rootPath = Path.of(myPath);
        Iterator<Path> itr = null;

        try {
            itr = Files.walk(rootPath).iterator();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        while (true) {
            try {
                if (itr.hasNext()) {
                    System.out.println(itr.next());
                } else {
                    break;
                }
            } catch (Exception e) {
                System.out.println(e.getLocalizedMessage());
            }
        }
    }
}
