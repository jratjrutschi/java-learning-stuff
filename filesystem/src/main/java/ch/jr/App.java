package ch.jr;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;

public class App {
    public static void main(String[] args) {
        new MyFileModifier("C:\\Users\\U112458\\OneDrive - Mobiliar Versicherungsgesellschaft AG\\Dokumente\\10 Dev\\", "kong-515_alte-faelle.txt").modify();

//        String myPath = "C:\\Users\\U112458\\OneDrive - Mobiliar Versicherungsgesellschaft AG\\Dokumente";
//
//        Path rootPath = Path.of(myPath);
//        try {
//            Files.walkFileTree(rootPath, new MyVisitor());
//        } catch (IOException e) {
//            throw new RuntimeException(e);
//        }
//
//        new MyWalkVisitor(myPath);
    }
}

class MyVisitor implements FileVisitor<Path> {

    // TODO "How FileVisitor works: https://www.baeldung.com/java-nio2-file-visitor

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
//        System.out.println(dir);
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
//        System.out.println(file);
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) {

        System.out.println("error on: " + file + " " + exc.getClass());
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
        return FileVisitResult.CONTINUE;
    }
}
