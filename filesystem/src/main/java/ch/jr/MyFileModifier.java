package ch.jr;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Scanner;

public class MyFileModifier {
    String path;
    String filename;

    public MyFileModifier(String path, String filename) {
        if (Files.exists(Path.of(path + filename))) {
            this.path = path;
            this.filename = filename;
        } else {
            System.out.println(" --- ERROR - Filepath '" + path + filename + "' not found");
        }

    }

    public void modify() {
        // neue Datei erstellen für Ergebnis
        String targetFile = path + "kong-515_alte-faelle-target.txt";
        FileWriter fw = null;
        Scanner myReader = null;

        try {
            myReader = new Scanner(new File(this.path + this.filename));
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        int counter = 0;

        try {
            fw = new FileWriter(targetFile);

            while (myReader.hasNextLine()) {
                counter++;
                fw.append("(1, '" + myReader.nextLine() + "')," + System.lineSeparator()); // (1, VALUE_2),
            }

            System.out.println("Anzahl Zeilen gelesen: " + counter);

            fw.close();
            myReader.close();
        } catch (IOException e) {
            System.out.println(" --- Schande! ... " + Arrays.toString(e.getStackTrace()));
        }
    }
}
