package ch.jr;

import org.apache.avro.Schema;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import example.avro.User;

public class AppTest 
{

    @Test
    public void testAvroSchema() {
        //-- arrange
        User user = User.newBuilder()
                .setName("Hansruedi")
                .setFavoriteNumber(13)
                .setFavoriteColor("blue")
                .build();

        //-- act
        Schema avroSchema = user.getSchema();

        //-- assert
        assertEquals("example.avro.User", avroSchema.getFullName());
        assertEquals("Schema for learning avro: User", avroSchema.getDoc());
    }
}
