# Module Generieren

`mvn archetype:generate -DgroupId=ch.jr -DartifactId=avro`

## Informationen

Um die Code - Generierung nutzen zu können (for performing code generation) wird das `Avro Maven plugin` benötigt.

Avro Schematas werden in JSON formuliert.

### Konfiguration des Avro Maven Plugins

Hier geht es vor allem um die Quelle der Avro-Schemata. Die Maven-Variable `${basedir}` führt zum base directory des Maven
Sub-Modules! Anstatt wie in den Mobiliar-Services `src/main/resources/avroschemas` zu konfigurieren. --> **PRÜFEN!!!**

${basedir}/src/main/resources/avroschemas

### output values to the console in the Maven build lifecycles.

(Extra-)Informationen während des Maven build lifecycles ausgeben kann praktisch sein, um z.B. herauszufinden, was z.B. in der
Mven-Variabeln ${basedir} steht. Es kann auch einfach Text ausgegeben werden!

Das Maven-Modul `maven` enthält weitere Beispiele.

## Offenes

- [x] führt `${basedir}/resources/avroschemas` zum selben Resultat wie `src/main/resources/avroschemas`? Nein, die Variante `${basedir}/ressources/avroschema` enthält schlussendlich den gesamten Pfad (z.B. `C:\ieu\development\zen\java-learning-stuff\avro/src/main/resources/avroschemas`) **Entscheid**: kürzere Variante `src/main/resources/avroschemas` verwenden!
